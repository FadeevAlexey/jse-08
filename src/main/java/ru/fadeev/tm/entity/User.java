package ru.fadeev.tm.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Role;

@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper=true)
public final class User extends AbstractEntity {

    @Nullable
    private String name ="";

    @Nullable
    private String password = "";

    @NotNull
    private Role role = Role.USER;

}