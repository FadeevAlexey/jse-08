package ru.fadeev.tm;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.command.application.AboutCommand;
import ru.fadeev.tm.command.application.ExitCommand;
import ru.fadeev.tm.command.application.HelpCommand;
import ru.fadeev.tm.command.project.*;
import ru.fadeev.tm.command.task.*;
import ru.fadeev.tm.command.user.*;
import ru.fadeev.tm.context.Bootstrap;

public final class Application {

    @NotNull
    private static final Class<?>[] CLASSES = {
            AboutCommand.class, ExitCommand.class, HelpCommand.class,
            ProjectClearCommand.class, ProjectCreateCommand.class,
            ProjectEditCommand.class, ProjectListCommand.class,
            ProjectRemoveCommand.class, ProjectTasksCommand.class,
            TaskAddProjectCommand.class, TaskClearCommand.class,
            TaskCreateCommand.class, TaskEditCommand.class,
            TaskListCommand.class, TaskRemoveCommand.class,
            UserCreateCommand.class, UserEditCommand.class,
            UserLoginCommand.class, UserProfileCommand.class,
            UserUpdatePasswordCommand.class, UserLogoutCommand.class
    };

    public static void main(String[] args) {
        new Bootstrap().init(CLASSES);
    }

}