package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeAll(@NotNull final String userId) {
        findAll().stream()
                .filter(task -> userId.equals(task.getUserId()))
                .forEach(task -> remove(task.getId()));
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId) {
        return findAll().stream()
                .filter(task ->userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId) {
        return findAll().stream()
                .filter(task -> projectId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public String findIdByName(@NotNull final String name, @NotNull final String userId) {
       @NotNull final Optional<Task> optionalProject = super.findAll()
                .stream()
                .filter(task ->
                        name.equals(task.getName()) && userId.equals(task.getUserId()))
                .findAny();
        return optionalProject.map(Task::getId).orElse(null);
    }

    @Override
    public void removeAllByProjectId(@NotNull String projectId, @NotNull String userId) {
        findAll(userId)
                .stream()
                .filter(task -> projectId.equals(task.getProjectId()))
                .forEach(task -> remove(task.getId()));
    }

    @Override
    public void removeAllProjectTask(@NotNull String userId) {
        findAll(userId)
                .forEach(task -> {
                    if (task.getProjectId() != null && !task.getProjectId().isEmpty())
                        remove(task.getId());
                });
    }

}