package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.entity.Project;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void removeAll(@NotNull final String userId) {
        findAll().stream()
                .filter(project -> userId.equals(project.getUserId()))
                .forEach(project -> remove(project.getId()));
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId){
      return findAll().stream()
                .filter(project -> userId.equals(project.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public String findIdByName(@NotNull String name, @NotNull final String userId) {
       @NotNull final Optional<Project> optionalProject = super.findAll()
                .stream()
                .filter(project ->
                        name.equals(project.getName()) && userId.equals(project.getUserId()))
                .findAny();
        return optionalProject.map(Project::getId).orElse(null);
    }

}