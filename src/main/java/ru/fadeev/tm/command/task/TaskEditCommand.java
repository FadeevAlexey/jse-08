package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.IllegalTaskNameException;
import ru.fadeev.tm.api.service.ITaskService;

import java.util.Date;

public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        terminal.print("[TASK EDIT]");
        terminal.print("ENTER CURRENT NAME:");
        @Nullable final String name = terminal.readString();
        @Nullable final String taskId = taskService.findIdByName(name, serviceLocator.getAppStateService().getUserId());
        if (taskId == null)
            throw new IllegalTaskNameException("Can't find task");
        @Nullable final Task task = taskService.findOne(taskId);
        if (task == null)
            throw new IllegalTaskNameException("Can't find task");
        fillFields(task);
        taskService.merge(task);
        terminal.print("[OK]\n");
        terminal.print("WOULD YOU LIKE ADD PROJECT TO TASK ? USE COMMAND task-addProject\n");
    }

    private void fillFields(@NotNull Task task) {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.print("YOU CAN ADD EDIT DESCRIPTION OR PRESS ENTER");
        @Nullable final String description = terminal.readString();
        terminal.print("YOU CAN ADD EDIT START DATE OR PRESS ENTER");
        @Nullable final Date startDate = terminal.readDate();
        terminal.print("YOU CAN ADD EDIT FINISH DATE OR PRESS ENTER");
        @Nullable final Date finishDate = terminal.readDate();
        if (description != null && !description.isEmpty()) task.setDescription(description);
        if (startDate != null) task.setStartDate(startDate);
        if (finishDate != null) task.setFinishDate(finishDate);
    }

}