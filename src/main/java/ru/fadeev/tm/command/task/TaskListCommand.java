package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Task;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final String currentUserId = serviceLocator.getAppStateService().getUserId();
        terminal.print("[TASK LIST]");
        int index = 1;
        for (@NotNull final Task task : serviceLocator.getTaskService().findAll(currentUserId))
            terminal.print(String.format("%d. Task(name: %s, description: %s, startDate: %s, finishDate: %s)",
                    index++, task.getName(),task.getDescription(),
                    terminal.dateToString(task.getStartDate()),
                    terminal.dateToString(task.getFinishDate())));
       terminal.print("");
    }

}