package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.IllegalTaskNameException;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final String userId = serviceLocator.getAppStateService().getUserId();
        terminal.print("[TASK CREATE]");
        terminal.print("ENTER NAME:");
        @Nullable final String name = terminal.readString();
        if (name == null || name.isEmpty())
            throw new IllegalTaskNameException("name can't be empty");
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        serviceLocator.getTaskService().persist(task);
        terminal.print("[OK]\n");
        terminal.print("WOULD YOU LIKE EDIT PROPERTIES TASK? USE COMMAND task-edit\n");
    }

}