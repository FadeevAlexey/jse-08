package ru.fadeev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "logout from task-manager";
    }

    @Override
    public void execute() {
        serviceLocator.getAppStateService().setUser(null);
        serviceLocator.getTerminalService().print("[OK]\n");
    }

}