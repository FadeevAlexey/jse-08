package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Project;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.api.service.IProjectService;

import java.util.Date;

public final class ProjectEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @Nullable final String currentUserId = serviceLocator.getAppStateService().getUserId();
        terminal.print("[EDIT PROJECT]");
        terminal.print("ENTER CURRENT NAME:");
        @Nullable final String projectId = projectService.findIdByName(terminal.readString(), currentUserId);
        if (projectId == null)
            throw new IllegalProjectNameException("Can't find project");
        @Nullable final Project project = projectService.findOne(projectId);
        if (project == null)
            throw new IllegalProjectNameException("Can't find project");
        fillFields(project);
        projectService.merge(project);
        terminal.print("[OK]\n");
    }

    private void fillFields(@NotNull final Project project) {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        terminal.print("YOU CAN ADD EDIT DESCRIPTION OR PRESS ENTER");
        @Nullable final String description = terminal.readString();
        terminal.print("YOU CAN ADD EDIT START DATE OR PRESS ENTER");
        @Nullable final Date startDate = terminal.readDate();
        terminal.print("YOU CAN ADD EDIT FINISH DATE OR PRESS ENTER");
        @Nullable final Date finishDate = terminal.readDate();
        if (description != null && !description.isEmpty()) project.setDescription(description);
        if (startDate != null) project.setStartDate(startDate);
        if (finishDate != null) project.setFinishDate(finishDate);
    }

}