package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.exception.IllegalProjectNameException;
import ru.fadeev.tm.api.service.IProjectService;
import ru.fadeev.tm.api.service.ITaskService;

public final class ProjectTasksCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-tasks";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all tasks inside project.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final String currentUserId = serviceLocator.getAppStateService().getUserId();
        terminal.print("[PROJECT TASKS]");
        terminal.print("ENTER PROJECT NAME");
        @Nullable final String projectId = projectService.findIdByName(terminal.readString(), currentUserId);
        if (projectId == null)
            throw new IllegalProjectNameException("Can't find project");
        int index = 1;
        for (@NotNull final Task task : taskService.findAll(currentUserId)) {
            if (projectId.equals(task.getProjectId()))
                terminal.print(String.format("%d. Task(name: %s, description: %s, startDate: %s, finishDate: %s)",
                        index++, task.getName(), task.getDescription(),
                        terminal.dateToString(task.getStartDate()),
                        terminal.dateToString(task.getFinishDate())));
        }
        terminal.print("");
    }

}