package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.command.AbstractCommand;

public final class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        @NotNull final String currentUserId = serviceLocator.getAppStateService().getUserId();
        serviceLocator.getProjectService().removeAll(currentUserId);
        serviceLocator.getTaskService().removeAllProjectTask(currentUserId);
        serviceLocator.getTerminalService().print("[ALL PROJECTS REMOVE]\n");
    }

}