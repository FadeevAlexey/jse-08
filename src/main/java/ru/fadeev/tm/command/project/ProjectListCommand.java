package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.Project;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @NotNull final String currentUserId = serviceLocator.getAppStateService().getUserId();
        terminal.print("[PROJECT LIST]");
        int index = 1;
        for (@NotNull final Project project : serviceLocator.getProjectService().findAll(currentUserId))
            terminal.print(String.format("%d. Project(name: %s, description: %s, startDate: %s, finishDate: %s)",
                    index++, project.getName(),project.getDescription(),
                    terminal.dateToString(project.getStartDate()),
                    terminal.dateToString(project.getFinishDate())));
        terminal.print("");
    }

}