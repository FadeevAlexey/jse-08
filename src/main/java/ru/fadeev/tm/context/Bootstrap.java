package ru.fadeev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IProjectRepository;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.*;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.CommandCorruptException;
import ru.fadeev.tm.exception.IllegalCommandNameException;
import ru.fadeev.tm.repository.*;
import ru.fadeev.tm.service.*;
import ru.fadeev.tm.util.HashUtil;
import ru.fadeev.tm.service.TerminalService;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final IAppStateService appStateService = new AppStateService();

    @Getter
    @NotNull
    private final ITerminalService terminalService = new TerminalService();

    public void init(@NotNull final Class<?>[] commands) {
        initCommand(commands);
        initUser();
        start();
    }

    private void initCommand(@NotNull final Class<?>[] commands) {
        for (@NotNull Class<?> command : commands) {
            if (command.isAssignableFrom(AbstractCommand.class))
                throw new CommandCorruptException();
            try {
                registry((AbstractCommand) command.newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                throw new CommandCorruptException();
            }
        }
    }

    private void initUser() {
        @NotNull final User user = new User();
        user.setName("User");
        user.setPassword(HashUtil.stringToMd5Hash("user"));
        @NotNull final User admin = new User();
        admin.setName("Admin");
        admin.setPassword(HashUtil.stringToMd5Hash("admin"));
        admin.setRole(Role.ADMINISTRATOR);
        userService.persist(user);
        userService.persist(admin);
    }

    private void start() {
        terminalService.print("*** WELCOME TO TASK MANAGER ***");
        @Nullable String command = "";
        while (!"exit".equals(command)) {
            try {
                command = getTerminalService().readString();
                execute(command);
            } catch (final IllegalArgumentException e) {
                terminalService.print("oops something went wrong: " + e.getMessage());
            } catch (final RuntimeException e) {
                terminalService.print(e.getMessage());
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        @Nullable final String cliCommand = command.getName();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        appStateService.putCommand(cliCommand, command);
    }

    private void execute(@Nullable final String command) {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = getAppStateService().getCommand(command);
        if (abstractCommand == null)
            throw new IllegalCommandNameException("Wrong command name");
        if (!checkPermission(abstractCommand))
            throw new AccessDeniedException("Access denied");
        abstractCommand.execute();
    }

    private boolean checkPermission(@NotNull final AbstractCommand abstractCommand) {
        @NotNull final boolean isPermission = abstractCommand.isPermission(appStateService.getUser());
        if (abstractCommand.accessRole() == null)
            return isPermission;
        return isPermission && getAppStateService().hasPermission(abstractCommand.accessRole());
    }

}