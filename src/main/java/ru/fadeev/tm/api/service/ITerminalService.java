package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface ITerminalService {

    @Nullable
    String readString();

    @Nullable
    Date readDate();

    @NotNull
    String dateToString(@Nullable final Date date);

    void print(@Nullable String string);

}
