package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IService<T> {

    @Nullable
    List<T> findAll();

    @Nullable
    T findOne(@Nullable final String id);

    @Nullable
    T remove(@Nullable final String id);

    @Nullable
    T persist(@Nullable final T t);

    @Nullable
    T merge(@Nullable final T t);

    void removeAll();

}