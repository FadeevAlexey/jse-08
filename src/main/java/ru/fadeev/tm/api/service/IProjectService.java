package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project> {

    @Nullable
    String findIdByName(@Nullable final String name, @Nullable final String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    void removeAll(@Nullable String userId);

}
