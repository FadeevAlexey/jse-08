package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;

public interface ITaskService extends IService<Task> {

    void removeAll(@Nullable final String userId);

    @NotNull
    Collection<Task> findAll(@Nullable final String userId);

    @Nullable
    String findIdByName(@Nullable String name, @Nullable String userId);

    void removeAllByProjectId(@Nullable final String projectId, @Nullable final String userId);

    void removeAllProjectTask(@Nullable final String userId);

}