package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    boolean isLoginExist(@NotNull final String login);

    @Nullable
    User findUserByLogin(@NotNull final String login);

}