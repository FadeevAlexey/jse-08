package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void removeAll(@NotNull final String userId);

    @NotNull
    List<Project> findAll(@NotNull final String userId);

    @Nullable
    String findIdByName(@NotNull final String name, @NotNull final String userId);

}
