package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITerminalService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class TerminalService implements ITerminalService {

    @NotNull
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Nullable
    public String readString(){
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public Date readDate(){
        @Nullable final String stringDate = readString();
        if (stringDate == null || stringDate.isEmpty())
            return null;
        if (stringDate.matches("\\d{2}.\\d{2}.\\d{4}")) {
            try {
                return dateFormat.parse(stringDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @NotNull
    public String dateToString(@Nullable final Date date) {
        if (date == null)
            return "";
        return dateFormat.format(date);
    }

    public void print(@Nullable String string){
        if (string == null)
            System.out.println("");
        System.out.println(string);
    }

}