package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.api.repository.ITaskRepository;

import java.util.ArrayList;
import java.util.Collection;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAll(userId);
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        return taskRepository.findAll(userId);
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String name, @Nullable final String userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        return taskRepository.findIdByName(name,userId);
    }

    @Override
    public void removeAllByProjectId(@Nullable final String projectId, @Nullable final String userId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllByProjectId(projectId,userId);
    }

    @Override
    public void removeAllProjectTask(@Nullable final String userId){
        if (userId == null || userId.isEmpty()) return;
        taskRepository.removeAllProjectTask(userId);
    }

}